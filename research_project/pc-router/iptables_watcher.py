#!/usr/bin/python3

import time
import subprocess
import pyinotify
import sqlite_db
import requests
import yaml

# LOG FILE
LOG_FILE = '/home/wacko/Documents/csc/791/dynamic-network/research_project/pc-router/iptables.log'
log = open(LOG_FILE, 'r')

# NETWORK OWNER CONFIG
OWNER_IP = yaml.load(open('./config.yaml', 'r'))['OWNER_IP']
OWNER_ENDPOINT = "http://{}:3000/request".format(OWNER_IP)

# DEVICE MAP (only one for now)
DEVICE_IP = "192.168.7.2"
DEVICE_MAP = {'5000': '192.168.7.2'}

class ProcessTransientFile(pyinotify.ProcessEvent):

    def __init__(self, *args, **kwargs):
        super(ProcessTransientFile, self).__init__(*args, **kwargs)
        self.position = 0

    def get_line(self):
        new_lines = log.read()
        last_n = new_lines.rfind('\n')
        if last_n >= 0:
            self.position += last_n + 1
            return new_lines[:last_n]
        else:
            return None
        self.file.seek(self.position)

    def process_IN_MODIFY(self, event):
        # We have explicitely registered for this kind of event.
        while True:
            line = self.get_line()
            if not line:
                # mod not needed, incomplete
                print ('skip mod')
                break
            ip_request = line.strip().split()
            print (len(ip_request))
            print (ip_request)
            if ip_request:
                src = None
                dest = None
                dport = None
                try:
                    for part in ip_request:
                        if "src" in part.lower():
                            src = part.split("=")[1]
                        elif "dst" in part.lower():
                            dest = part.split("=")[1]
                        elif "dpt" in  part.lower():
                            dport = part.split("=")[1]
                except Exception:
                    print ("Error parsing")
                    break

                if not src or not dest or not dport:
                    continue

                # Determine port -> device IP mapping
                print (dport)
                print(DEVICE_MAP.get(dport))
                if DEVICE_MAP.get(dport):
                    real_dest = DEVICE_MAP[dport]
                else:
                    continue

                # Check if route is already known
                route = sqlite_db.get_route(src, "{}:{}".format(real_dest, dport))
                # IF known, either has access and should already be configured (rare case)
                # or no access is given and should be ignored
                if route:
                    continue
                else:
                    # Send request to net-owner to add
                    resp = requests.post(OWNER_ENDPOINT + "/{}-{}:{}".format(src, real_dest, dport))

                print (resp.text)
                print(src)
                print(dest + ":" + dport)
                print()


    def process_default(self, event):
        # Implicitely IN_CREATE and IN_DELETE are watched too. You can
        # ignore them and provide an empty process_default or you can
        # process them, either with process_default or their dedicated
        # method (process_IN_CREATE, process_IN_DELETE) which would
        # override process_default.
        print ('default: ', event.maskname)



if __name__== '__main__':
    sqlite_db.start()
    wm = pyinotify.WatchManager()
    notifier = pyinotify.Notifier(wm)
    # In this case you must give the class object (ProcessTransientFile)
    # as last parameter not a class instance.
    wm.watch_transient_file(LOG_FILE, pyinotify.IN_MODIFY, ProcessTransientFile)
    notifier.loop()
