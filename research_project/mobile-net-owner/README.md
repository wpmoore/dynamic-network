# dynamic-network


## Mobile Net Owner

One example of this authorization scheme utilizes a network owner's smart phone to
allow management of network requests.  We had experimented with Android Studio but deemed
too much effort and not aligned with the underlying goals of the research.  Therefore, a
simpler web-http interface was used (under /net-owner).
