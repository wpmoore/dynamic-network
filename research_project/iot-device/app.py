#!/bin/python


from flask import Flask
app = Flask(__name__)

@app.route('/iot-device/getData', methods=['GET'])
def get_data():
    return 'Hello, World!\n'


if __name__== '__main__':
    # Expose IoT device HTTP endpoint on private network
    app.run(host='192.168.7.2', port=5000)
