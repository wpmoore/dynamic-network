#!/bin/bash

# MAC OS has a bad date command - extended via coreutils from brew via gdate
if [[ "$OSTYPE" == "darwin"* ]]; then
  date_util="gdate"
else
  date_util="date"
fi


ROUTER_IP="192.168.3.2"
ROUTER_NET="192.168.3"
DEVICE_ENDPOINT="http://$ROUTER_IP:5000/iot-device/getData"

NUMBER_DELAYS=250
CLIENT_OWNER_DELAYS=()


for i in $(seq 1 $NUMBER_DELAYS); do
    let HOST=$i+2
    # # Change network address to simulate a different client
    sudo ifconfig enp0s25 $ROUTER_NET.$HOST netmask 255.255.255.0 up
    # Time start when first request is attempted
    START_TIME="$($date_util -u +%s.%N)"
    while true; do
        # Attempt device http server for data
        curl -X GET $DEVICE_ENDPOINT
        STATUS=$?
        if test $STATUS -eq 0
        then
            # echo "Successfuly connection to device"
            # Quit as soon as we succeed
            break
        # else
            # echo "Failed to connect to device"
            # sleep 0.1
        fi
    done
    # Time ends after successfuly connecting to device
    END_TIME="$($date_util -u +%s.%N)"
    elapsed="$(bc <<<"$END_TIME-$START_TIME")"
    CLIENT_OWNER_DELAYS+=($elapsed)
done



# EXPORT DELAYS
for i in $(seq 1 $NUMBER_DELAYS); do
  let index=$i-1

  echo -n "${CLIENT_OWNER_DELAYS[$index]}" >> results.csv
  echo "" >> results.csv
done
