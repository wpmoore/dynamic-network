#!/bin/bash

#
# DEMO SCRIPT
#
# We can use this to test throughout development, and then we will have a script
# already prepared which we can use for the demo
#
# USAGE: bash$  ./demo.sh
# This will create two separate windows which run the client and server processes
#
#

###############
# LINUX - ROUTER
###############
if [ "$1" == "pc-router-app" ]
then
   # Run  process for router HTTP server
   echo "Run process for router HTTP server"
   echo "python3 ./pc-router/app.py"
   echo ""
   python3 ./pc-router/app.py
   sleep 30
fi

if [ "$1" == "pc-router-watcher-ip" ]
then
   # Run process for log file watcher
   echo "Run process for log file watcher"
   echo "python3 ./pc-router/iptables_watcher.py"
   echo ""
   python3 ./pc-router/iptables_watcher.py
   sleep 30
fi

if [ "$1" == "pc-router-watcher-logs" ]
then
   # Run process on pc-router to filter syslogs into a iptables log
   echo "Run process on pc-router to filter syslogs into a iptables log"
   make watchlogs
   sleep 30
fi
#############################


###############################
# LINUX - NETWORK OWNER DEVICE
###############################
# MUST RUN SEPARATELY ON DEVICE
if [ "$1" == "net-owner-app" ]
then
   # Run process for HTTP server
   python3 ./net-owner/app.py
   sleep 30
fi
##############################


#######
# BBBK
#######
if [ "$1" == "device" ]
then
   # Run server process
   echo "Kill any previous process and start iot-device http server"
   kill $(ps -elf | grep app | awk '{print $4}')
   ssh root@192.168.7.2 python /root/iot-device/app.py
   sleep 30
fi
################




if [ "$1" == "" ]
then
  echo "Starting processes";
  # Create two separate processes for server and client
  gnome-terminal -e './demo.sh device &'
  sleep 5
  gnome-terminal -e './demo.sh pc-router-app &'
  sleep 1
  gnome-terminal -e './demo.sh pc-router-watcher-logs &'
  gnome-terminal -e './demo.sh pc-router-watcher-ip &'

fi


if [ "$1" == "watcher" ]
then
  echo "Starting processes";
  # Create two separate processes
  gnome-terminal -e './demo.sh pc-router-watcher-logs &'
  gnome-terminal -e './demo.sh pc-router-watcher-ip &'

fi
