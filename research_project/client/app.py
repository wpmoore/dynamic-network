#!/bin/python


###############################################
#   VM - CLIENT APPLICATION / CONTROLLER
###############################################

from flask import Flask
import requests
import yaml



app = Flask(__name__)

ROUTER_IP = yaml.load(open('./config.yaml', 'r'))['ROUTER_IP']
DEVICE_ENDPOINT = "http://{}:5000/iot-device/getData".format(ROUTER_IP)


@app.route('/vm-client/requestAccess', methods=['GET'])
def request_access():

    resp = requests.get(DEVICE_ENDPOINT)
    return resp.text


if __name__== '__main__':
    # Expose client HTTP endpoint for remote initiation of tests, and
    # remote retirieval of results.
    app.run(host='0.0.0.0', port=5001)
