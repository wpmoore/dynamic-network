from flask import Flask, flash, redirect, render_template, request, session, abort, url_for
from random import randint
import requests
import os
import yaml

app = Flask(__name__)

active_requests = []
ROUTER_IP = yaml.load(open('./config.yaml', 'r'))['ROUTER_IP']
ROUTER_ENDPOINT = "http://" + ROUTER_IP + ":5003/pc-router"

#####################
# HELPER FUNCITONS
#####################
def list_to_table(net_requests):
    tbl_str = "<table border=1>"
    tbl_str += "<tr><th>ID</th><th>Client IP Address</th><th>Requested Device IP Address</th>"
    tbl_str += "<th>Accept</th><th>Deny</th></tr>"
    id = 0
    for req in net_requests:
        id += 1

        acpt_btn = "accept"
        deny_btn = "deny"
        tbl_str += "<tr><td>{}</td><td>{}</td><td>{}</td><td>{}</td><td>{}</td></tr>".format(id,
                    src, dest, acpt_btn, deny_btn)
    tbl_str += "</table>"
    return tbl_str


@app.route("/")
def index():
    return "Network Owner HTTP Server is healthy\n"

@app.route("/request/<string:request_string>/", methods=['POST'])
def add(request_string):
    # Extract info from request string
    req_parts = request_string.split('-')
    src = req_parts[0]
    dest = req_parts[1]
    req = {'id': len(active_requests) + 1, 'src': src, 'dest': dest}

    # Check same request hasn't been added already
    for r in active_requests:
        if r['src'] == req['src'] and r['dest'] == req['dest']:
            break
    else:
        active_requests.append(req)

    # TESTING PURPOSES
    # Always accept, so ID always zero
    resp = requests.post(ROUTER_ENDPOINT + "/accept/{}/".format(request_string))

    print (resp.text)


    return "Request Added", 200

@app.route("/requests", methods=['GET'])
def show():
    return render_template(
        'index.html',active_requests=active_requests)


@app.route("/accept/<string:id>", methods=['GET'])
def accept(id):
    req = active_requests.pop(int(id) - 1)
    req_str = "{}-{}".format(req['src'], req['dest'])
    resp = requests.post(ROUTER_ENDPOINT + "/accept/{}/".format(req_str))

    print (resp.text)
    return redirect('/requests')

@app.route("/deny/<string:id>", methods=['GET'])
def deny(id):
    req = active_requests.pop(int(id) - 1)
    req_str = "{}-{}".format(req['src'], req['dest'])
    resp = requests.post(ROUTER_ENDPOINT + "/deny/{}/".format(req_str))

    print (resp.text)
    return redirect('/requests')


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=3000)
