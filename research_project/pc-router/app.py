#!/bin/python


###############################################
#   PC - ROUTER APPLICATION / CONTROLLER
###############################################

from flask import Flask, request
import requests
import sqlite_db
import subprocess
import yaml

app = Flask(__name__)

CLIENT_ROUTER_IP = yaml.load(open('./config.yaml', 'r'))['CLIENT_ROUTER_IP']
DEVICE_ENDPOINT = '192.168.7.2:5000/iot-device/getData'

@app.route("/", methods=['GET'])
def index():
    return "Router HTTP Server is healthy\n"

@app.route('/pc-router/accept/<string:req_str>/', methods=['POST'])
def accept_access(req_str):

    parts = req_str.split('-')
    src = parts[0]
    dest = parts[1]
    dport = dest.split(':')[1]

    # Add to sqlite DB
    sqlite_db.add_route(src, dest, 1)

    # Add IPtables rule
    base_rule = "sudo iptables -t nat -A PREROUTING "
    ip_rule = " -s {0} -p tcp -d {1} --dport {2} -j DNAT --to-destination {3}".format(src, CLIENT_ROUTER_IP, dport, dest)
    returned_value = subprocess.call(base_rule + ip_rule, shell=True)
    print (returned_value)
    print (base_rule + ip_rule)

    return "Access Granted", 200

@app.route('/pc-router/deny/<string:req_str>/', methods=['POST'])
def deny_access(req_str):

    parts = req_str.split('-')
    src = parts[0]
    dest = parts[1]

    # Add to sqlite DB
    sqlite_db.add_route(src, dest, 0)
    print (src + " " + dest)

    return "Access Denied", 200

if __name__== '__main__':
    # Expose client HTTP endpoint for remote initiation of tests, and
    # remote retirieval of results.
    sqlite_db.start()
    app.run(host='0.0.0.0', port=5003)
