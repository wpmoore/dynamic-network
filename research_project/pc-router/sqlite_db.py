#!/bin/python3
'''
    File to contain functions for statically working with DB.

    Cost of getting SQLite DB Connection is cheap, we will avoid
    having to maintain an open connection in program.
'''


import sqlite3
import logging

DB_URL = '/home/wacko/Documents/csc/791/dynamic-network/research_project/pc-router/router.db'
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

def get_db():
    '''
        Get DB connection,
        will create DB if not already made.

        RETURNS:    DB connection handle for subsequent use
    '''
    conn = None
    try:
        conn = sqlite3.connect(DB_URL)
    except Exception as err:
        logger.error("Error getting DB. Error: " + str(err))

    return conn

def start(forceCreate=False):
    '''
        Will ensure DB has been created,
        will create tables if own hostname is not found.

        RETURNS:    True if the db is ready to use
    '''
    db_check = get_route('db-check', 'default')
    if not forceCreate and db_check and db_check[0] == 'db-check':
        logger.info("DB has already been initialized, reusing")
    else:
        logger.info("Creating new DB")
        initilize_db()


################################
#   GENERIC WRAPPER FUNCITONS
################################

def execute(sql_cmd, params_obj):
    ''' Add a host to the server users table '''
    c = get_db()
    conn = c.cursor()
    try:
        conn.execute(sql_cmd, params_obj)
    except Exception as error:
        logger.error(str(error))
        logger.error("Failed to execute:\n %s\nParams: %s",
                     str(sql_cmd), str(params_obj))
    conn.close()
    c.commit()
    c.close()

def fetch_one(sql_cmd, params_obj):
    '''
        Run arbitrary command then fetch one

        RETURNS:  False if command couldn't be executed
    '''
    c = get_db()
    conn = c.cursor()
    try:
        conn.execute(sql_cmd, params_obj)
        ret = conn.fetchone()
    except Exception as error:
        conn.close()
        c.close()
        logger.error(str(error))
        logger.error("Failed to fetch one from query:\n %s\nParams: %s",
                     str(sql_cmd), str(params_obj))
        return None

    conn.close()
    c.close()
    return ret

def fetch_all(sql_cmd, params_obj):
    '''
        Run arbitrary command then fetch all

        RETURNS:  False if command couldn't be executed
    '''
    c = get_db()
    conn = c.cursor()
    try:
        if params_obj:
            conn.execute(sql_cmd, params_obj)
        else:
            conn.execute(sql_cmd)
        ret = conn.fetchall()
    except Exception as error:
        conn.close()
        c.close()
        logger.error(str(error))
        logger.error("Failed to fetch all from query:\n %s\nParams: %s",
                     str(sql_cmd), str(params_obj))
        return None

    conn.close()
    c.close()
    return ret


def get_route(src, device):
    ''' Get a route object '''
    params = (src, device,)
    sql = 'SELECT * FROM routes WHERE source_ip=? AND device_addr=?'
    return fetch_one(sql, params)

def add_route(src, dest, access):
    ''' Add a file to the server files table '''
    params = (src, dest, access,)
    sql = 'INSERT INTO routes (source_ip, device_addr, access) values (?, ?, ?)'
    execute(sql, params)

def initilize_db():
    ''' Create tables, and set initial information as needed '''
    c = get_db()
    conn = c.cursor()
    # Remove any tables left over
    conn.execute('DROP TABLE IF EXISTS routes')

    # Users Table
    conn.execute('''CREATE TABLE routes (source_ip text, device_addr text, access integer, PRIMARY KEY(source_ip, device_addr))''')

    # Add server as initial user (will help determine if we need to re-create db later)
    system_host = ('db-check', "default", 0, )
    conn.execute('''INSERT INTO routes(source_ip, device_addr, access) values (?, ?, ?)''', system_host)

    conn.close()
    c.commit()
    c.close()
